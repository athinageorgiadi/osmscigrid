import os
from osmscigrid.CSV import read
from qplot.qplot import quickplot
#from check import correct_segments
TM_World_Borders_file='../TM_World_Borders/TM_WORLD_BORDERS-0.3.shp'

if __name__=="__main__":
    countrycode='EU'

    INET = read('Data/',NetzName='INET',
               NameStart=('INET'))

    INET2=INET.reduce(countrycode)
    fig1=quickplot((INET2),
               Cursor_Lines=True,
               Cursor_Points=False,
               Legend=True,
               LegendStyle="Str(Num)",
               #parameter='_',
               orig_path=True,
               #thicklines=True,
               #parameter='max_pressure_bar',
               #parameter='diameter_mm',
               #thicklines=True,
               #SingleColor='red',
               #SingleLineStyle='solid',
               hold=False,
               countrycode=countrycode,TM_Borders_filename=TM_World_Borders_file)
 

